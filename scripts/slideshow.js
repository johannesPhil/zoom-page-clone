let slides = document.querySelectorAll(".slide");
let slideIndex = 0;
let slideIndicator = document.querySelectorAll(".dot");
let controlBtn = document.querySelector(".control-img");
let playing = true;
let slideInterval = setInterval(slider, 6000);

//Features display on hover variables
let cards = document.querySelectorAll(".feature-card");
let icons = document.querySelectorAll(".icon");

function slider() {
  for (let i = 0; i < slides.length; i++) {
    slides[i].classList.remove("visible");
  }

  slideIndex++;
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }
  for (let i = 0; i < slideIndicator.length; i++) {
    slideIndicator[i].classList.remove("active");
  }
  slides[slideIndex - 1].classList.add("visible");
  slideIndicator[slideIndex - 1].classList.add("active");
}

function playSlide() {
  controlBtn.src = "/assets/button-pause.png";
  playing = true;
  slideInterval = setInterval(slider, 6000);
}

function pauseSlide() {
  controlBtn.src = "/assets/button-play.png";
  playing = false;
  clearInterval(slideInterval);
}

controlBtn.addEventListener("click", function () {
  if (playing) {
    pauseSlide();
  } else {
    playSlide();
  }
});

slideIndicator.forEach((indicator) => {
  indicator.addEventListener("click", (e) => {
    for (let i = 0; i < slides.length; i++) {
      slides[i].classList.remove("visible");
    }
    for (let i = 0; i < slideIndicator.length; i++) {
      slideIndicator[i].classList.remove("active");
    }
    slides[e.target.dataset.index].classList.add("visible");
    slideIndicator[e.target.dataset.index].classList.add("active");
  });
});

icons.forEach((icon) => {
  icon.addEventListener("mouseenter", (e) => {
    for (let i = 0; i < cards.length; i++) {
      cards[i].classList.remove("hovered");
    }
    // document.querySelector(".foryou").style.opacity = 0;
    cards[e.target.dataset.card].classList.add("hovered");
  });
});

//Reviews

let reviews = document.querySelectorAll(".review");
let reviewIndex = 0;
let reviewInterval = setInterval(reviewSlide, 3000);
let reviewThumbs = document.querySelectorAll(".thumb");

function reviewSlide() {
  for (let i = 0; i < reviews.length; i++) {
    reviews[i].classList.remove("slide-in");
  }
  reviewIndex++;
  if (reviewIndex > reviews.length) {
    reviewIndex = 1;
  }
  for (let i = 0; i < reviewThumbs.length; i++) {
    reviewThumbs[i].classList.remove("active-review");
  }
  reviews[reviewIndex - 1].classList.add("slide-in");
  reviewThumbs[reviewIndex - 1].classList.add("active-review");
}

reviewThumbs.forEach((thumb) => {
  thumb.addEventListener("mouseenter", (e) => {
    for (let i = 0; i < reviews.length; i++) {
      reviews[i].classList.remove("slide-in");
    }
    for (let i = 0; i < reviewThumbs.length; i++) {
      reviewThumbs[i].classList.remove("active-review");
    }
    reviews[e.target.dataset.thumb].classList.add("slide-in");
    reviewThumbs[e.target.dataset.thumb].classList.add("active-review");
  });
});

//Brands slideshow
let brands = document.querySelectorAll(".brands");
let brandsIndicator = document.querySelectorAll(".indicator");
let brandIndex = 0;
let brandsCategory = document.querySelector(".brand-category__text");
let brandInterval = setInterval(brandSlide, 5000);

function brandSlide() {
  setTimeout(() => {
    for (let i = 0; i < brands.length; i++) {
      brands[i].classList.remove("active-brands");
    }

    for (let i = 0; i < brandsIndicator.length; i++) {
      brandsIndicator[i].classList.remove("active-indicator");
    }

    brandIndex++;

    if (brandIndex > brands.length) {
      brandIndex = 1;
    }

    brands[brandIndex - 1].classList.add("active-brands");

    console.log(brands[brandIndex - 1].dataset.sectorpledge);

    brandsCategory.innerHTML = brands[brandIndex - 1].dataset.sectorpledge;

    brandsIndicator[brandIndex - 1].classList.add("active-indicator");
  }, 2000);
}

brandsIndicator.forEach((indicator) => {
  indicator.addEventListener("click", (e) => {
    for (let i = 0; i < brands.length; i++) {
      brands[i].classList.remove("active-brands");
    }

    for (let i = 0; i < brandsIndicator.length; i++) {
      brandsIndicator[i].classList.remove("active-indicator");
    }

    brandsIndicator[e.target.dataset.brands].classList.add("active-indicator");
    brands[e.target.dataset.brands].classList.add("active-brands");
  });
});

//footer dropdowns
let select = document.querySelector(".language__option");
let dropDown = document.querySelector(".dropdown-menu");
let option = document.querySelector(".option");
// select.addEventListener("click", (e) => {});

window.addEventListener("click", function (e) {
  e.target == select || e.target == option
    ? dropDown.classList.remove("invisible")
    : dropDown.classList.add("invisible");
});
