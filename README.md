# Zoom page clone

A clone of zoom.us homepage featuring the interactive aspects of the page

## Getting started

1. Clone the repository from (here)[https://gitlab.com/johannesPhil/zoom-page-clone]
2. Run the command below to install the development dependencies

```
npm install
```

3. Open two terminal tabs and run `npm sass` and `npm run lite` on each of them
